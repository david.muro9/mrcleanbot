﻿using MrCleanBot.Contracts;
using MrCleanBot.Contracts.Configuration;
using MrCleanBot.Models;
using System;

namespace MrCleanBot.Implementation
{
    public class CleanerBot : ICleanerBot
    {
        private const string PositionOutOfBoundsMessage = "I cannot move there!";

        private readonly IGridConfiguration gridConfiguration;

        public Coordinate CurrentPosition { get; private set; }

        public CleanerBot(
            IGridConfiguration gridConfiguration,
            Coordinate initialPosition)
        {
            this.gridConfiguration = 
                gridConfiguration ?? throw new ArgumentNullException(nameof(gridConfiguration));

            this.CurrentPosition =
                initialPosition ?? throw new ArgumentNullException(nameof(initialPosition));
        }

        public void Move(CompassPoint compassPoint, int steps)
        {
            if (steps < 1)
            {
                throw new ArgumentException();
            }

            switch (compassPoint)
            {
                case CompassPoint.N:
                    MoveY(steps);
                    break;
                case CompassPoint.S:
                    MoveY(-steps);
                    break;
                case CompassPoint.W:
                    MoveX(-steps);
                    break;
                case CompassPoint.E:
                    MoveX(steps);
                    break;
            }
        }

        private void MoveX(int steps)
        {
            var futurePosition = (Coordinate)this.CurrentPosition.Clone();

            futurePosition.X += steps;

            if (futurePosition.X > this.gridConfiguration.MaximumX ||
                futurePosition.X < this.gridConfiguration.MinimumX)
            {
                throw new ArgumentOutOfRangeException(PositionOutOfBoundsMessage);
            }

            this.CurrentPosition = futurePosition;
        }

        private void MoveY(int steps)
        {
            var futurePosition = (Coordinate)this.CurrentPosition.Clone();

            futurePosition.Y += steps;

            if (futurePosition.Y > this.gridConfiguration.MaximumY ||
                futurePosition.Y < this.gridConfiguration.MinimumY)
            {
                throw new ArgumentOutOfRangeException(PositionOutOfBoundsMessage);
            }

            this.CurrentPosition = futurePosition;
        }
    }
}
