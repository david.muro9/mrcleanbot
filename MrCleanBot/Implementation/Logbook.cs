﻿using MrCleanBot.Contracts;
using MrCleanBot.Models;
using System.Collections.Generic;

namespace MrCleanBot.Implementation
{
    public class Logbook : ILogbook
    {
        private readonly HashSet<Coordinate> uniquePlaces;

        public IEnumerable<Coordinate> Entries =>
            this.uniquePlaces as IEnumerable<Coordinate>;

        public Logbook()
        {
            this.uniquePlaces = new HashSet<Coordinate>(new PointComparer());
        }

        public void Add(Coordinate coordinate)
        {
            this.uniquePlaces.Add(coordinate);
        }
    }

    internal class PointComparer : IEqualityComparer<Coordinate>
    {
        public bool Equals(Coordinate coordinate1, Coordinate coordinate2)
        {
            return coordinate1.X == coordinate2.X && coordinate1.Y == coordinate2.Y;
        }

        public int GetHashCode(Coordinate obj)
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + obj.X.GetHashCode();
                hash = hash * 23 + obj.Y.GetHashCode();
                return hash;
            }
        }
    }
}
