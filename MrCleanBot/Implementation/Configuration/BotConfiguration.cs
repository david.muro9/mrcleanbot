﻿using MrCleanBot.Contracts.Configuration;

namespace MrCleanBot.Implementation.Configuration
{
    public class BotConfiguration : IBotConfiguration
    {
        public int MaxDistance => 10000;
    }
}
