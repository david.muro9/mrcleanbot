﻿using MrCleanBot.Contracts.Configuration;

namespace MrCleanBot.Implementation.Configuration
{
    public class GridConfiguration : IGridConfiguration
    {
        public int MaximumX => 100000;

        public int MinimumX => -100000;

        public int MaximumY => 100000;

        public int MinimumY => -100000;
    }
}
