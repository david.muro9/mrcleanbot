﻿using MrCleanBot.Contracts;
using MrCleanBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MrCleanBot.Implementation
{
    public class MrClean : IMrClean
    {
        private readonly ICleanerBot cleanerBot;
        private readonly ILogbook logbook;

        public MrClean(
            ICleanerBot cleanerBot,
            ILogbook logbook)
        {
            this.cleanerBot =
                cleanerBot ?? throw new ArgumentNullException(nameof(cleanerBot));

            this.logbook =
                logbook ?? throw new ArgumentNullException(nameof(logbook));
        }

        public int ExecuteCleaning(IEnumerable<Leg> legs)
        {
            this.logbook.Add(this.cleanerBot.CurrentPosition);

            foreach (var leg in legs)
            {
                while (leg.Distance-- > 0)
                {
                    this.cleanerBot.Move(leg.CompassPoint, 1);
                    this.logbook.Add(this.cleanerBot.CurrentPosition);
                }
            }

            return this.logbook.Entries.Count();
        }
    }
}
