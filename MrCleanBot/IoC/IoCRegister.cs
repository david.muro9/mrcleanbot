﻿using LightInject;
using MrCleanBot.Contracts;
using MrCleanBot.Contracts.Configuration;
using MrCleanBot.Implementation;
using MrCleanBot.Implementation.Configuration;
using MrCleanBot.Models;

namespace MrCleanBot.IoC
{
    public class IoCRegister
    {
        private static bool isRegistered = false;

        public static void Register(ServiceContainer container, Coordinate start)
        {
            if (!isRegistered)
            {
                container.RegisterInstance<Coordinate>(start);

                container.Register<IGridConfiguration, GridConfiguration>();
                container.Register<ICleanerBot, CleanerBot>();
                container.Register<ILogbook, Logbook>();
                container.Register<IMrClean, MrClean>();
            }

            isRegistered = true;
        }
    }
}
