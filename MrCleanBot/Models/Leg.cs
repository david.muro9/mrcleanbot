﻿namespace MrCleanBot.Models
{
    public class Leg
    {
        public CompassPoint CompassPoint { get; set; }

        public int Distance { get; set; }
    }
}
