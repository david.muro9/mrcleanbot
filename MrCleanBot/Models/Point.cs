﻿using System;

namespace MrCleanBot.Models
{
    public class Coordinate : ICloneable
    {
        public int X { get; set; }

        public int Y { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
