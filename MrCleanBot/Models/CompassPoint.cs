﻿namespace MrCleanBot.Models
{
    public enum CompassPoint
    {
        N,
        S,
        W,
        E
    }
}
