﻿using MrCleanBot.Models;

namespace MrCleanBot.Contracts
{
    /// <summary>
    /// Provides a way to interact with cleaning bots.
    /// </summary>
    public interface ICleanerBot
    {
        /// <summary>
        /// Gets the current position of the bot.
        /// </summary>
        Coordinate CurrentPosition { get; }

        /// <summary>
        /// Move the bot towards a compass point.
        /// </summary>
        /// <param name="compassPoint">Compass direction the bot must head to.</param>
        /// <param name="steps">Number of steps to be taken by the bot in a
        /// given compass direction. The number must be greater than zero.</param>
        void Move(CompassPoint compassPoint, int steps);
    }
}
