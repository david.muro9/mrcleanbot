﻿namespace MrCleanBot.Contracts.Configuration
{
    /// <summary>
    /// Defines the configuration a cleaning bot has.
    /// </summary>
    public interface IBotConfiguration
    {
        /// <summary>
        /// Maximum distance the bot can make for one leg.
        /// </summary>
        int MaxDistance { get; }
    }
}
