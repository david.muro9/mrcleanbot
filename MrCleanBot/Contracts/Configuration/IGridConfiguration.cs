﻿namespace MrCleanBot.Contracts.Configuration
{
    /// <summary>
    /// Provides the limits to which the bot can move.
    /// </summary>
    public interface IGridConfiguration
    {
        /// <summary>
        /// Defines the right limit of a grid.
        /// </summary>
        int MaximumX { get; }

        /// <summary>
        /// Defines the left limit of a grid.
        /// </summary>
        int MinimumX { get; }

        /// <summary>
        /// Defines the top limit of a grid.
        /// </summary>
        int MaximumY { get; }

        /// <summary>
        /// Defines the bottom limit of a grid.
        /// </summary>
        int MinimumY { get; }
    }
}
