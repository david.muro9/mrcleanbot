﻿using MrCleanBot.Models;
using System.Collections.Generic;

namespace MrCleanBot.Contracts
{
    /// <summary>
    /// Stores unique coordinates that the bot visited.
    /// </summary>
    public interface ILogbook
    {
        /// <summary>
        /// Stores a coordinate in the log. If the coordinate already exist, it does not add it.
        /// </summary>
        /// <param name="coordinate">A coordinate visited by the bot.</param>
        void Add(Coordinate coordinate);

        /// <summary>
        /// Gets all the unique entries in the logbook.
        /// </summary>
        IEnumerable<Coordinate> Entries { get; }
    }
}
