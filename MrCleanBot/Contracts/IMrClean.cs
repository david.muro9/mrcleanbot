﻿using MrCleanBot.Models;
using System.Collections.Generic;

namespace MrCleanBot.Contracts
{
    /// <summary>
    /// Provides a way to perform a cleaning.
    /// </summary>
    public interface IMrClean
    {
        /// <summary>
        /// Performs the cleaning from a given set of legs.
        /// </summary>
        /// <param name="legs">The commands MrClean must follow to clean the office.</param>
        /// <returns>The unique places MrClean has cleaned.</returns>
        int ExecuteCleaning(IEnumerable<Leg> legs);
    }
}
