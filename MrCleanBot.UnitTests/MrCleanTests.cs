﻿using AutoFixture.Xunit2;
using FluentAssertions;
using MrCleanBot.Contracts;
using MrCleanBot.Implementation;
using MrCleanBot.Models;
using MrCleanBot.UnitTests.Utils;
using NSubstitute;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace MrCleanBot.UnitTests
{
    public class MrCleanTests
    {
        [Theory, AutoMockData]
        public void ExecuteCleaning_CallsBot(
            [Center]Coordinate center)
        {
            // Arrange
            var legs = new List<Leg>
            {
                new Leg
                {
                    CompassPoint = CompassPoint.N,
                    Distance = 1,
                },
            };

            var cleanerBot = Substitute.For<ICleanerBot>();
            cleanerBot.CurrentPosition
                .ReturnsForAnyArgs(center);

            var sut = new MrClean(cleanerBot, Substitute.For<ILogbook>());

            // Act
            sut.ExecuteCleaning(legs);

            // Assert
            cleanerBot
                .Received()
                .Move(Arg.Any<CompassPoint>(), Arg.Any<int>());
        }

        [Theory, AutoMockData]
        public void ExecuteCleaning_WhenRouteIsNDistanceLong_BotMovesCorrectDistance(
            [Center]Coordinate center)
        {
            // Arrange
            var legs = new List<Leg>
            {
                new Leg
                {
                    CompassPoint = CompassPoint.N,
                    Distance = 1,
                },
                new Leg
                {
                    CompassPoint = CompassPoint.E,
                    Distance = 2,
                },
            };

            var expectedMoves = legs.Sum(x => x.Distance);

            var cleanerBot = Substitute.For<ICleanerBot>();
            cleanerBot.CurrentPosition
                .ReturnsForAnyArgs(center);

            var sut = new MrClean(cleanerBot, Substitute.For<ILogbook>());

            // Act
            sut.ExecuteCleaning(legs);

            // Assert
            cleanerBot
                .Received(expectedMoves)
                .Move(Arg.Any<CompassPoint>(), Arg.Any<int>());
        }

        [Theory, AutoMockData]
        public void ExecuteCleaning_WhenStraightRoute_ReturnsUniquePlacesVisited(
            [Frozen]ICleanerBot cleanerBot)
        {
            // Arrange
            const int ExpectedUniquePlacesVisited = 2;

            var route = CreateLinearRoute(CompassPoint.N, 1);

            cleanerBot.CurrentPosition.Returns(
                x => new Coordinate { X = 0, Y = 0 },
                x => new Coordinate { X = 0, Y = 1 });

            var sut = new MrClean(cleanerBot, new Logbook());

            // Act
            var actual = sut.ExecuteCleaning(route);

            // Assert
            actual.Should().Be(ExpectedUniquePlacesVisited);
        }

        [Theory, AutoMockData]
        public void ExecuteCleaning_WhenSquaredRoute_ReturnsUniquePlacesVisited(
            [Frozen]ICleanerBot cleanerBot)
        {
            // Arrange
            const int ExpectedUniquePlacesVisited = 4;

            var route = CreateSquaredRoute(1);

            cleanerBot.CurrentPosition.Returns(
                x => new Coordinate { X = 0, Y = 0 },
                x => new Coordinate { X = 0, Y = 1 },
                x => new Coordinate { X = 1, Y = 1 },
                x => new Coordinate { X = 1, Y = 0 },
                x => new Coordinate { X = 0, Y = 0 });

            var sut = new MrClean(cleanerBot, new Logbook());

            // Act
            var actual = sut.ExecuteCleaning(route);

            // Assert
            actual.Should().Be(ExpectedUniquePlacesVisited);
        }

        #region Private Methods

        private static IEnumerable<Leg> CreateLinearRoute(CompassPoint direction, int distance)
        {
            return new List<Leg>
            {
                new Leg
                {
                    CompassPoint = direction,
                    Distance = distance,
                },
            };
        }

        private static IEnumerable<Leg> CreateSquaredRoute(int side)
        {
            return new List<Leg>
            {
                new Leg
                {
                    CompassPoint = CompassPoint.N,
                    Distance = side,
                },
                new Leg
                {
                    CompassPoint = CompassPoint.E,
                    Distance = side,
                },
                new Leg
                {
                    CompassPoint = CompassPoint.S,
                    Distance = side,
                },
                new Leg
                {
                    CompassPoint = CompassPoint.W,
                    Distance = side,
                }
            };
        }

        #endregion
    }
}
