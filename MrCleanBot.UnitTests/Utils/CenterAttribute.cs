﻿using AutoFixture;
using AutoFixture.Xunit2;
using MrCleanBot.Models;
using System.Reflection;

namespace MrCleanBot.UnitTests.Utils
{
    public class CenterAttribute : CustomizeAttribute
    {
        public override ICustomization GetCustomization(ParameterInfo parameter)
        {
            var fixture = new Fixture();
            fixture.Inject(
                new Coordinate
                {
                    X = 0,
                    Y = 0
                });

            return fixture.ToCustomization();
        }
    }
}
