﻿using AutoFixture;
using AutoFixture.AutoNSubstitute;
using AutoFixture.Xunit2;
using System;

namespace MrCleanBot.UnitTests.Utils
{
    public class AutoMockDataAttribute : AutoDataAttribute
    {
        public AutoMockDataAttribute()
            : base(new Fixture().Customize(new AutoNSubstituteCustomization()))
        {

        }
    }
}
