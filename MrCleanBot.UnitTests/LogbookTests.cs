﻿using FluentAssertions;
using MrCleanBot.Implementation;
using MrCleanBot.Models;
using MrCleanBot.UnitTests.Utils;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace MrCleanBot.UnitTests
{
    public class LogbookTests
    {
        [Theory, AutoMockData]
        public void Add_WhenNewEntry_AddsTheEntry(
            [Center]Coordinate coordinate,
            Logbook sut)
        {
            // Act
            sut.Add(coordinate);

            // Assert
            sut.Entries.Should().Contain(coordinate);
        }

        [Theory, AutoMockData]
        public void GetEntries_WhenAddedDuplicated_ReturnsOneEntry(
            [Center]Coordinate coordinate,
            [Center]Coordinate duplicatedCoordinate,
            Logbook sut)
        {
            // Arrange
            var expectedEntries = new List<Coordinate> { coordinate };

            sut.Add(coordinate);
            sut.Add(duplicatedCoordinate);

            // Act
            var actual = sut.Entries;

            // Assert
            actual.Count().Should().Be(1);
            actual.Should().BeEquivalentTo(expectedEntries);
        }
    }
}
