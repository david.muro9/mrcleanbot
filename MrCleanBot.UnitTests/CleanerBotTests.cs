using FluentAssertions;
using MrCleanBot.Contracts.Configuration;
using MrCleanBot.Implementation;
using MrCleanBot.Models;
using MrCleanBot.UnitTests.Utils;
using NSubstitute;
using System;
using Xunit;

namespace MrCleanBot.UnitTests
{
    public class CleanerBotTests
    {
        [Theory, AutoMockData]
        public void Position_WhenMoves_PositionIsUpdated(
            [Center]Coordinate startingPoint)
        {
            // Arrange
            var expectedPoint = new Coordinate
            {
                X = 0,
                Y = 1,
            };

            var gridConfiguration = CreateDefaultGridConfiguration();

            var sut = new CleanerBot(gridConfiguration, startingPoint);

            sut.Move(CompassPoint.N, 1);

            // Act
            var actual = sut.CurrentPosition;

            // Assert
            actual.Should().BeEquivalentTo(expectedPoint);
        }

        [Theory, AutoMockData]
        public void Move_WhenHeadsNorthAndIsOutOfRange_ThrowsError(
            [Center]Coordinate startingPoint)
        {
            // Arrange
            var gridConfiguration = Substitute.For<IGridConfiguration>();
            gridConfiguration.MaximumY.Returns(1000);

            var sut = new CleanerBot(gridConfiguration, startingPoint);

            // Act
            Action act = (() => sut.Move(CompassPoint.N, 200000));

            // Assert
            act.Should().Throw<ArgumentOutOfRangeException>();
        }

        [Theory, AutoMockData]
        public void Move_WhenHeadsSouthAndIsOutOfRange_ThrowsError(
            [Center]Coordinate startingPoint)
        {
            // Arrange
            var gridConfiguration = Substitute.For<IGridConfiguration>();
            gridConfiguration.MinimumY.Returns(-1000);

            var sut = new CleanerBot(gridConfiguration, startingPoint);

            // Act
            Action act = (() => sut.Move(CompassPoint.S, 200000));

            // Assert
            act.Should().Throw<ArgumentOutOfRangeException>();
        }

        [Theory, AutoMockData]
        public void Move_WhenHeadsEastAndIsOutOfRange_ThrowsError(
            [Center]Coordinate startingPoint)
        {
            // Arrange
            var gridConfiguration = Substitute.For<IGridConfiguration>();
            gridConfiguration.MaximumX.Returns(1000);

            var sut = new CleanerBot(gridConfiguration, startingPoint);

            // Act
            Action act = (() => sut.Move(CompassPoint.E, 200000));

            // Assert
            act.Should().Throw<ArgumentOutOfRangeException>();
        }

        [Theory, AutoMockData]
        public void Move_WhenHeadsWestAndIsOutOfRange_ThrowsError(
            [Center]Coordinate startingPoint)
        {
            // Arrange
            var gridConfiguration = Substitute.For<IGridConfiguration>();
            gridConfiguration.MinimumX.Returns(-1000);

            var sut = new CleanerBot(gridConfiguration, startingPoint);

            // Act
            Action act = (() => sut.Move(CompassPoint.W, 200000));

            // Assert
            act.Should().Throw<ArgumentOutOfRangeException>();
        }

        [Theory, AutoMockData]
        public void Move_WhenHeadsNorth_BotMovesOk(
            [Center]Coordinate startingPoint)
        {
            // Arrange
            var expectedPosition = new Coordinate
            {
                X = 0,
                Y = 4,
            };

            var gridConfiguration = CreateDefaultGridConfiguration();

            var sut = new CleanerBot(gridConfiguration, startingPoint);

            // Act
            sut.Move(CompassPoint.N, 4);

            // Assert
            sut.CurrentPosition.Should().BeEquivalentTo(expectedPosition);
        }

        [Theory, AutoMockData]
        public void Move_WhenHeadsSouth_BotMovesOk([Center]Coordinate startingPoint)
        {
            // Arrange
            var expectedPosition = new Coordinate
            {
                X = 0,
                Y = -10,
            };

            var gridConfiguration = CreateDefaultGridConfiguration();

            var sut = new CleanerBot(gridConfiguration, startingPoint);

            // Act
            sut.Move(CompassPoint.S, 10);

            // Assert
            sut.CurrentPosition.Should().BeEquivalentTo(expectedPosition);
        }

        [Theory, AutoMockData]
        public void Move_WhenHeadsEast_BotMovesOk([Center]Coordinate startingPoint)
        {
            // Arrange
            var expectedPosition = new Coordinate
            {
                X = 50,
                Y = 0,
            };

            var gridConfiguration = CreateDefaultGridConfiguration();

            var sut = new CleanerBot(gridConfiguration, startingPoint);

            // Act
            sut.Move(CompassPoint.E, 50);

            // Assert
            sut.CurrentPosition.Should().BeEquivalentTo(expectedPosition);
        }

        [Theory, AutoMockData]
        public void Move_WhenHeadsWest_BotMovesOk([Center]Coordinate startingPoint)
        {
            // Arrange
            var expectedPosition = new Coordinate
            {
                X = -50,
                Y = 0,
            };

            var gridConfiguration = CreateDefaultGridConfiguration();

            var sut = new CleanerBot(gridConfiguration, startingPoint);

            // Act
            sut.Move(CompassPoint.W, 50);

            // Assert
            sut.CurrentPosition.Should().BeEquivalentTo(expectedPosition);
        }

        [Theory, AutoMockData]
        public void Move_WhenMoveIsOk_DoesNotThrow([Center]Coordinate startingPoint)
        {
            // Arrange
            var compassPoint = CompassPoint.N;

            var gridConfiguration = CreateDefaultGridConfiguration();

            var sut = new CleanerBot(gridConfiguration, startingPoint);

            // Act
            Action act = (() => sut.Move(compassPoint, 1));

            // Assert
            act.Should().NotThrow();
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void Move_WhenDistanceIsLessThanOne_ThrowsError(int distance)
        {
            // Arrange
            var startingPoint = new Coordinate
            {
                X = 100,
                Y = 100,
            };

            var compassPoint = CompassPoint.N;

            var gridConfiguration = CreateDefaultGridConfiguration();

            var sut = new CleanerBot(gridConfiguration, startingPoint);

            // Act
            Action act = (() => sut.Move(compassPoint, distance));

            // Assert
            act.Should().Throw<ArgumentException>();
        }

        #region Private Methods

        private static IGridConfiguration CreateDefaultGridConfiguration()
        {
            var gridConfiguration = Substitute.For<IGridConfiguration>();

            gridConfiguration.MaximumX.Returns(500);
            gridConfiguration.MinimumX.Returns(-500);

            gridConfiguration.MaximumY.Returns(500);
            gridConfiguration.MinimumY.Returns(-500);

            return gridConfiguration;
        }

        #endregion
    }
}
