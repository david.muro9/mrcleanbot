﻿using LightInject;
using MrCleanBot.Contracts;
using MrCleanBot.Models;
using System;
using System.Collections.Generic;

namespace MrCleanBot.Ui
{
    public class Program
    {
        private static void Main(string[] args)
        {
            var legCount = GetLegCount();
            var startingCoordinates = GetStartingCoordinates();
            var route = GetRoute(legCount);

            var mrClean = CreateMrClean(startingCoordinates);

            var result = mrClean.ExecuteCleaning(route);

            Console.Write($"=> Cleaned: {result}");
        }

        private static IMrClean CreateMrClean(Coordinate startingCoordinates)
        {
            var container = new ServiceContainer();

            MrCleanBot.IoC.IoCRegister.Register(container, startingCoordinates);

            var mrClean = container.GetInstance<IMrClean>();

            return mrClean;
        }

        private static IEnumerable<Leg> GetRoute(int legCount)
        {
            var legs = new List<Leg>();

            for (var i = 0; i < legCount; i += 1)
            {
                var command = Console.ReadLine().Split(' ');
                var compassPoint =
                    (CompassPoint)Enum.Parse(typeof(CompassPoint), command[0]);
                var times = Convert.ToInt32(command[1]);

                var leg = new Leg
                {
                    CompassPoint = compassPoint,
                    Distance = times
                };

                legs.Add(leg);
            }

            return legs;
        }

        private static int GetLegCount()
        {
            return Convert.ToInt32(Console.ReadLine());
        }

        private static Coordinate GetStartingCoordinates()
        {
            var startingCoordinates = Console.ReadLine().Split(' ');

            var coordinate = new Coordinate
            {
                X = Convert.ToInt32(startingCoordinates[0]),
                Y = Convert.ToInt32(startingCoordinates[1])
            };

            return coordinate;
        }
    }
}
